# **Traffic Sign Recognition Report** 


---

**Build a Traffic Sign Recognition Project**

The goals / steps of this project are the following:
* Load the data set (see below for links to the project data set)
* Explore, summarize and visualize the data set
* Design, train and test a model architecture
* Use the model to make predictions on new images
* Analyze the softmax probabilities of the new images
* Summarize the results with a written report


[//]: # (Image References)

[image1]: ./img/histogram.png "Visualization"
[image2]: ./img/prep.png "Grayscaling"
[image3]: ./img/notpre.png "Random Noise"
[image4]: ./img/trafficsigns.png "Traffic Sign 1"
[image5]: ./img/1-30speed.png "Traffic Sign 2"
[image6]: ./img/12-priority-road.png "Traffic Sign 3"
[image7]: ./img/13-yield.png "Traffic Sign 4"
[image8]: ./img/20-roundabout-mandatory.png "Traffic Sign 5"
[image9]: ./img/Comparison-of-Adam-to-Other-Optimization-Algorithms-Training-a-Multilayer-Perceptron.png "Adam Optimzer" 

## Rubric Points
### Here I will consider the [rubric points](https://review.udacity.com/#!/rubrics/481/view) individually and describe how I addressed each point in my implementation.  

---

### Data Set Summary & Exploration

#### 1. Summary of the data set.

I used the pandas library to calculate summary statistics of the traffic
signs data set:

* The size of training set is 34799
* The size of the validation set is 4410
* The size of test set is 12630
* The shape of a traffic sign image is (32,32,3) = (image_width, image_height, number_of_color_channels)
* The number of unique classes/labels in the data set is 43

#### 2. Include an exploratory visualization of the dataset.

Here is an exploratory visualization of the data set. It is an histogram chart showing how the data is distributed in the training set.

![alt text][image1]

### Design and Test a Model Architecture

#### 1. Describe how you preprocessed the image data. What techniques were chosen and why did you choose these techniques? Consider including images showing the output of each preprocessing technique. Pre-processing refers to techniques such as converting to grayscale, normalization, etc. (OPTIONAL: As described in the "Stand Out Suggestions" part of the rubric, if you generated additional data for training, describe why you decided to generate additional data, how you generated the data, and provide example images of the additional data. Then describe the characteristics of the augmented training set like number of images in the set, number of images for each class, etc.)

We started by converting the images to grayscale because it is easier to manipulate less channels (reduced from 3 to 1), it requires less computation time without any side effects on the performance.
Next, we moved to feature scaling which speeds up the process of calculating gradient decent to get the optimal weights. 
As a last step, we adjusted the contrast of the images through histogram equalization  

Here is an example of a traffic sign image before and after preprocessing.

![alt text][image3]
![alt text][image2]


#### 2. Describe what your final model architecture looks like including model type, layers, layer sizes, connectivity, etc.) Consider including a diagram and/or table describing the final model.

My final model consisted of the following layers:

| Layer         		|     Description	        				            |    Output Size                                        |
|:---------------------:|:-----------------------------------------------------:|:------------------------------------------------------| 
| Input         		| grayscale image   				                	| 32x32x1                                               |
| Convolution       	|with relu; size: 5x5; stride:1;Depth:32;keep_prob=0.9  | 32x32x32                                              |                                             
| Max pooling	      	| size: 2x2 stride: 2                    	        	| 32x32x1                                               |   
| Convolution       	|with relu; size: 5x5; stride:1;Depth:64;keep_prob=0.8  | 16x16x64                                              |                                             
| Max pooling	      	| size: 2x2 stride: 2                    		        | 16x16x32                                              |            
| Convolution       	|with relu; size: 5x5; stride:1;Depth:64;keep_prob=0.7  | 8x8x64                                                |                                             
| Max pooling	      	| size: 2x2 stride: 2                 			        | 8x8x64                                                |            
| Fully connected		| No Relu    					                        | 256                                                   |
| Fully connected		| No Relu  					                            | 43                                                    |



#### 3. Describe how you trained your model. The discussion can include the type of optimizer, the batch size, number of epochs and any hyperparameters such as learning rate.


To train the model we have used 3 alternating convolutions and max pooling layers, this was done to ensure pattern recognition (lines and curves) in the different samples f data.
The convolution layers have respective dropouts of 0.1, 0.2 and 0.3 this was done in order to further improve the training process of the network.
for the activation function we have decided to work with Relu because it gives the best results, and the maxpooling was enabled in order to redue the compexity by selecting the most notable data.
after these six layers we have used two final fully connected layers with the last one including flatten to reduce the dimensions. 


### The Optimizer

We have used Adam's optimize because it is well suited for problems with large amounts of features and data, it is also very efficicient when it comes to memory equirements and computation time.


### Learning rate  

we have tested multiple learning rates before we settled for 0.001 which seemed to give the best result, as it is also the one recommended by adam optimizer.

### Batch size

The batch size has to be generally a power of two and it generally varies between 23 and 512 we have decided to work with 256 because it helps reduce the variance o the stochastic gradient decent hence giving as less time for convergence.


### Number of Epochs

similarly to the learning rate we have tested multiple number of epochs starting from a high number and we stopped at the number when we noticed that the loss function is no longer decreasing.
We have settled on a 100 epochs.


#### 4. Describe the approach taken for finding a solution and getting the validation set accuracy to be at least 0.93. Include in the discussion the results on the training, validation and test sets and where in the code these were calculated. Your approach may have been an iterative process, in which case, outline the steps you took to get to the final solution and why you chose those steps. Perhaps your solution involved an already well known implementation or architecture. In this case, discuss why you think the architecture is suitable for the current problem.

Our final model results were:
* training set accuracy of 1
* validation set accuracy of 0.98 
* test set accuracy of 0.963

If an iterative approach was chosen:
* What was the first architecture that was tried and why was it chosen?

The initial architechture was a simple input layer folowed up by a convolution and a maxpool layer. there were no dropouts and basically random hyper parameters set. the results were extremely poor.
* What were some problems with the initial architecture?

The accuracy was low on all the different sets (training, validation and test) we tried tuning all the different hyperparameters but there wher only slight improvements.
so we realized there was an underfitting problem so we needed to add more layers to our network. 

* How was the architecture adjusted and why was it adjusted? Typical adjustments could include choosing a different model architecture, adding or taking away layers (pooling, drpout, convolution, etc), using an activation function or changing the activation function. One common justification for adjusting an architecture would be due to overfitting or underfitting. A high accuracy on the training set but low accuracy on the validation set indicates over fitting; a low accuracy on both sets indicates under fitting.

We have decided to add two more layers of convolution and maxpooling we have also used dropout technique in the convolution layers we added another fully connected layer and the results immediately started looking better, we finished it up by changing our activation function frm sigmoid to relu which turned out to give better solutoins.
* Which parameters were tuned? How were they adjusted and why?

we have tuned the batch size and the number of epochs on multiple occasions in order to get better results when it came to the Cost. 
we have also tried varying the learning rate in order to accelerate the process of convergence but at some point gradient descent ended u diverging so we had to tune it back.

* What are some of the important design choices and why were they chosen? For example, why might a convolution layer work well with this problem? How might a dropout layer help with creating a successful model?

For this problem we immediately noticed that CNN's are the most suited because they try to find patterns in the input data. They stack them to make abstract concepts by there convolution layers. They try to find out whether the input data has each of these concepts or not in there dense layers to figure out which class the input data belongs to.
Using dropout indeed helps with this problem so we do not heavily rely on one pattern making the others invisible, for that reason we have to use random dropouts on the training set in order to combine all the patterns and get better results.

### Test a Model on New Images

#### 1. Choose five German traffic signs found on the web and provide them in the report. For each image, discuss what quality or qualities might be difficult to classify.

Here are seven German traffic signs that I found on the web:

![alt text][image4] 

The third and fourth images might be the most difficult to classify, for the third image is from a class that has little samples compared to the others and for the fourth image, the background colour can be easily confused with the colour of the actual sign.


#### 2. Discuss the model's predictions on these new traffic signs and compare the results to predicting on the test set. At a minimum, discuss what the predictions were, the accuracy on these new predictions, and compare the accuracy to the accuracy on the test set (OPTIONAL: Discuss the results in more detail as described in the "Stand Out Suggestions" part of the rubric).

Here are the results of the prediction:

| Image			        |     Prediction	        					| 
|:---------------------:|:---------------------------------------------:| 
| Speed limit (30km/h)  | Speed limit (30km/h)   						| 
| Priority road     	| Priority road 								|
| Keep right			| Keep right									|
| Turn left ahead	    | Turn left ahead					 			|
| General caution		| General caution      							|
| Road work			    | Road work      							    |
| Stop			        | Stop      							        |

The model was able to correctly guess 7 of the 7 traffic signs, which gives an accuracy of a 100%. This compares favorably to the accuracy on the test set of 96.3%.

#### 3. Describe how certain the model is when predicting on each of the five new images by looking at the softmax probabilities for each prediction. Provide the top 5 softmax probabilities for each image along with the sign type of each probability. (OPTIONAL: as described in the "Stand Out Suggestions" part of the rubric, visualizations can also be provided such as bar charts)

The code for making predictions on my final model is located in the 19th cell of the Ipython notebook.

The model was almost 100% certain of each prediction it made for the 7 images, for example, for the first image, below are te top five softmax probabilities.

| Probability         	|     Prediction	        					| 
|:---------------------:|:---------------------------------------------:| 
| .99        			| Speed limit (30km/h)    						| 
| .003     				| Speed limit (80km/h)  						|
| .00002				| Roundabout mandatory							|
| .0000002	      		| Speed limit (60km/h)					 		|
| .00000001				| Go straight or right     						|

